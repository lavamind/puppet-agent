require 'spec_helper'
require 'tmpdir'

describe "test resource abstraction interface" do
  tmpdir = Dir.mktmpdir('foo')

  describe command("puppet resource file #{tmpdir}/foo ensure=file content=bar") do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match %r{Notice: /File\[#{tmpdir}/foo\]/ensure: defined content as} }

    describe file("#{tmpdir}/foo") do
      it { should be_file }
      its(:content) { should eq 'bar' }
    end
  end

  describe command("puppet resource file #{tmpdir} ensure=absent force=true recurse=true") do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match %r{Notice: /File\[#{tmpdir}\]/ensure: removed} }

    describe file("#{tmpdir}") do
      it { should_not exist }
    end
  end
end
